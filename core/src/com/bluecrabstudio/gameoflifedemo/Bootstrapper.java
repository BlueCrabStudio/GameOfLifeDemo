package com.bluecrabstudio.gameoflifedemo;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;

public class Bootstrapper extends ApplicationAdapter {
	private GameOfLifeActor gol;
	private GameOfLifeSettingsContent settingsContent;
	private Stage stage;
	private boolean showGame;
	private GolConfig config;
	
	@Override
	public void create () {
		stage = new Stage(new ExtendViewport(Math.min(900,Gdx.graphics.getWidth()), Math.min(1080,Gdx.graphics.getHeight())));
		Gdx.input.setInputProcessor(stage);
		if (Gdx.graphics.getWidth() >= 900 && Gdx.graphics.getHeight() >= 500) {
			VisUI.load(VisUI.SkinScale.X2);
		} else {
			VisUI.load();
		}
		config = new GolConfig();
		gol = new GameOfLifeActor(config);
		gol.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (showGame) {
					settingsContent.show();
					gol.hide();
					showGame=false;
				}
			}
		});
		settingsContent = new GameOfLifeSettingsContent(this);
		addUserInterface();
		showGame = true;
	}

	public void showGame(GolConfig config) {
		this.config = config;
		settingsContent.hide();
		this.showGame = true;
		gol.show(config);
	}
	
	public Stage getStage() {
		return stage;
	}
	
	private void addUserInterface() {
		VisTable table = new VisTable();
		table.setFillParent(true);
		table.add(gol).height(stage.getHeight()).width(stage.getWidth());
		
		stage.addActor(table);
	}

	@Override
	public void resize(int width, int height) {
		if (width == 0 && height == 0) return;
		stage.getViewport().update(width, height);
		stage.getViewport().apply(true);
	}
	
	@Override
	public void render () {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}
	
	@Override
	public void dispose () {
		VisUI.dispose();
		stage.dispose();
	}

	public GolConfig getConfig() {
		return config;
	}
}
