package com.bluecrabstudio.gameoflifedemo;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.util.TableUtils;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.color.ColorPicker;
import com.kotcrab.vis.ui.widget.color.ColorPickerAdapter;

public class ColorPickerWindow extends VisTable {
	private static Drawable white = VisUI.getSkin().getDrawable("white");
	private ColorPicker picker;
	private final VisImageTextButton imageButton;
	
	public ColorPickerWindow (Color oldColor) {
		imageButton = new VisImageTextButton(" Change ", white);
		imageButton.getImageCell().width(17);
		imageButton.getImageCell().height(17);
		imageButton.pad(0);
		imageButton.padLeft(3);
		
		picker = new ColorPicker("color picker", new ColorPickerAdapter() {
			@Override
			public void finished (Color newColor) {
				imageButton.getImage().setColor(newColor);
			}
		});

		imageButton.addListener(new ChangeListener() {
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				getStage().addActor(picker.fadeIn());
			}
		});

		Color c = oldColor;
		picker.setColor(c);
		imageButton.getImage().setColor(c);
		TableUtils.setSpacingDefaults(this);
		add(imageButton);
		pack();
	}
	
	public Color getColor() {
		return new Color(imageButton.getImage().getColor());
	}
}