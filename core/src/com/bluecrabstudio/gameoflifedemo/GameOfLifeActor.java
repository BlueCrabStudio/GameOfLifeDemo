package com.bluecrabstudio.gameoflifedemo;

import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class GameOfLifeActor extends Actor {
	private GameOfLifeModel model;
	private ShapeRenderer renderer;
	private GolConfig config;
	
	private static float unpopulatedColorRed;
	private static float unpopulatedColorGreen;
	private static float unpopulatedColorBlue;
	
	private static float deadColorRed;
	private static float deadColorGreen;
	private static float deadColorBlue;
	
	private static float livingColorRed;
	private static float livingColorGreen;
	private static float livingColorBlue;
	
	private float deltaTime;
	
	public GameOfLifeActor(GolConfig config) {
		model = new GameOfLifeModel(config);
		renderer = new ShapeRenderer();
		restart(config);
	}
	
	private void restart(GolConfig config) {
		this.config = config;
		model.restart(config);
		this.addAction(Actions.visible(true));
		deltaTime = 0;
		
		unpopulatedColorRed = config.getUnpopulatedColor().r;
		unpopulatedColorGreen = config.getUnpopulatedColor().g;
		unpopulatedColorBlue = config.getUnpopulatedColor().b;
		
		livingColorRed = config.getLivingColor().r;
		livingColorGreen = config.getLivingColor().g;
		livingColorBlue = config.getLivingColor().b;

		deadColorRed = config.getDeadColor().r;
		deadColorGreen = config.getDeadColor().g;
		deadColorBlue = config.getDeadColor().b;
		
		placeRandom();
	}
	
	public void hide() {
		this.addAction(Actions.visible(false));
	}
	
	public void show(GolConfig config) {
		this.restart(config);
	}
	
	private void placeRandom() {
		java.util.Random random = new Random(100);
		for (int i = 0; i<config.getSizeX(); i++) {
			for (int j = 0; j<config.getSizeY(); j++) {
				if (random.nextBoolean()) {
					model.setCreatureAlive(i, j, 100);
				} else {
					model.setCreatureAlive(i, j, -1);
				}
			}
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		float tileHeight = this.getHeight() / config.getSizeY();
		float tileWidth = this.getWidth() / config.getSizeX();
		renderer.setProjectionMatrix(this.getStage().getCamera().combined);
		renderer.begin(ShapeType.Filled);
		for (int i=0; i<config.getSizeX(); i++) {
			for (int j=0; j<config.getSizeY(); j++) {
				if (model.getCreatureValue(i, j) == 100) {
					renderer.setColor(new Color(livingColorRed, livingColorGreen, livingColorBlue, 1));
					renderer.rect(getX() + i*tileWidth, getY() + j*tileHeight, tileWidth, tileHeight);
				}
				
				if (model.getCreatureValue(i, j) == 0) {
					renderer.setColor(new Color(deadColorRed, deadColorGreen, deadColorBlue, 1));
					renderer.rect(getX() + i*tileWidth, getY() + j*tileHeight, tileWidth, tileHeight);
				}
				
				if (model.getCreatureValue(i, j) == -1) {
					renderer.setColor(new Color(unpopulatedColorRed, unpopulatedColorGreen, unpopulatedColorBlue, 1));
					renderer.rect(getX() + i*tileWidth, getY() + j*tileHeight, tileWidth, tileHeight);
				}
				
				if (config.isDrawHistory() && model.getCreatureValue(i, j) > 0 && model.getCreatureValue(i, j) < 100) {
					float grey = (float)((float)model.getCreatureValue(i, j)/100f);
					float newColorRed = config.getInterpolation().apply(deadColorRed, livingColorRed, grey);
					float newColorGreen = config.getInterpolation().apply(deadColorGreen, livingColorGreen, grey);
					float newColorBlue = config.getInterpolation().apply(deadColorBlue, livingColorBlue, grey);
					
					renderer.setColor(new Color(newColorRed, newColorGreen, newColorBlue, 1));
					renderer.rect(getX() + i*tileWidth, getY() + j*tileHeight, tileWidth, tileHeight);
				} 
			}
		}
		renderer.end();
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		deltaTime+=delta;
		if (deltaTime >= config.getSteptimeInSeconds()) {
			deltaTime = 0;
			model.performStep();
		}
	}
}
