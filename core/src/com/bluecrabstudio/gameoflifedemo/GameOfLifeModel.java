package com.bluecrabstudio.gameoflifedemo;

public class GameOfLifeModel {
	private int[][] map;
	private GolConfig config;
	
	public GameOfLifeModel(GolConfig config) {
		this.config = config;
		map = new int[config.getSizeX()][config.getSizeY()];
		for (int i=0; i<config.getSizeX(); i++) {
			for (int j=0; j<config.getSizeY(); j++) {
				map[i][j] = -1;
			}
		}
	}
	
	public void restart(GolConfig config) {
		this.config = config;
		map = new int[config.getSizeX()][config.getSizeY()];
		for (int i=0; i<config.getSizeX(); i++) {
			for (int j=0; j<config.getSizeY(); j++) {
				map[i][j] = -1;
			}
		}
	}
	
	public int getCreatureValue(int x, int y) {
		if (x < 0 || x >= config.getSizeX() || y < 0 || y >= config.getSizeY()) 
			return -1;
		return map[x][y];
	}
	
	public void setCreatureAlive(int x, int y, int alive) {
		if (x < 0 || x >= config.getSizeX() || y < 0 || y >= config.getSizeY()) 
			return;
		map[x][y] = alive;
	}
	
	private int getNumberOfLivingNeighbours(int x, int y) {
		int neighbours = 0;
		
		if (getCreatureValue(x, y+1) == 100)
			neighbours++;
		if (getCreatureValue(x+1, y+1)== 100)
			neighbours++;
		if (getCreatureValue(x+1, y)== 100)
			neighbours++;
		if (getCreatureValue(x+1, y-1)== 100)
			neighbours++;
		if (getCreatureValue(x, y-1)== 100)
			neighbours++;
		if (getCreatureValue(x-1, y-1)== 100)
			neighbours++;
		if (getCreatureValue(x-1, y)== 100)
			neighbours++;
		if (getCreatureValue(x-1, y+1)== 100)
			neighbours++;
		
		return neighbours;
	}
	
	public void performStep() {
		int[][] newmap = new int[config.getSizeX()][config.getSizeY()];
		
		for (int i=0; i<config.getSizeX(); i++) {
			for (int j=0; j<config.getSizeY(); j++) {

				if (map[i][j] >= 1) {
					newmap[i][j] = map[i][j]-1;
				}
				if (map[i][j] == -1) {
					newmap[i][j] = -1;
				}
				int neighbours = getNumberOfLivingNeighbours(i, j);
				
				if (getCreatureValue(i, j)== 100) {
					if (neighbours == 3 || neighbours == 2) {
						newmap[i][j] = 100;
					}
				} else {
					if (getNumberOfLivingNeighbours(i, j) == 3) {
						newmap[i][j] = 100;
					}					
				}
			}
		}
		
		this.map = newmap;
	}
	
	public int[][] getMap() {
		return this.map;
	}
}
