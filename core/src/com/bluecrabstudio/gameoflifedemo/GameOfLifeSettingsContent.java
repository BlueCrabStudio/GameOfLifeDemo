package com.bluecrabstudio.gameoflifedemo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSelectBox;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

public class GameOfLifeSettingsContent extends VisTable {
    private Bootstrapper bootstrapper;
    private GolConfig oldConfig;
    private Array<EntryOneToOne<String, Interpolation>> interpolationMap;
    private VisSlider xSlider;
    private VisSlider ySlider;
    private VisSlider nextStepTimeSlider;
    private VisCheckBox drawHistoryCheckBox;
    private VisSelectBox<String> colorInterpolationSelection;
    private ColorPickerWindow livingColorPicker, deadColorPicker, unusedColorPicker;
    private Skin skin;

    public GameOfLifeSettingsContent(Bootstrapper bootstrapper) {
        oldConfig = bootstrapper.getConfig();
        this.bootstrapper = bootstrapper;
        this.addAction(Actions.visible(false));
        bootstrapper.getStage().addActor(this);
        this.skin = new Skin(Gdx.files.internal("TestExport/GameOfLive.json"));
        buildUI();
    }

    private void buildUI() {
        this.setFillParent(true);
        this.setX(0);
        this.interpolationMap = new Array<EntryOneToOne<String, Interpolation>>();
        interpolationMap.add(new EntryOneToOne<String, Interpolation>("Linear", Interpolation.linear));
        interpolationMap.add(new EntryOneToOne<String, Interpolation>("Smooth", Interpolation.smooth));
        interpolationMap.add(new EntryOneToOne<String, Interpolation>("Exponential", Interpolation.exp10));
        this.defaults().pad(8).align(Align.left);

        final VisLabel sizeX = new VisLabel("size X: ");
        final VisLabel sizeXValue = new VisLabel("" + oldConfig.getSizeX());
        final VisLabel sizeY = new VisLabel("size Y: ");
        final VisLabel sizeYValue = new VisLabel("" + oldConfig.getSizeY());

        xSlider = new VisSlider(5, Gdx.graphics.getWidth(), 1, false);
        xSlider.setValue(oldConfig.getSizeX());
        xSlider.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (actor instanceof VisSlider) {
                    VisSlider slider = (VisSlider) actor;
                    sizeXValue.setText("" + (int) slider.getValue());
                }
            }
        });

        ySlider = new VisSlider(5, Gdx.graphics.getHeight(), 1, false);
        ySlider.setValue(oldConfig.getSizeY());
        ySlider.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (actor instanceof VisSlider) {
                    VisSlider slider = (VisSlider) actor;
                    sizeYValue.setText("" + (int) slider.getValue());
                }
            }
        });

        final VisLabel nextStepTime = new VisLabel("Time between steps (in milliseconds): ");
        final VisLabel nextStepTimeValue = new VisLabel("" + (int) Math.round(1000 * oldConfig.getSteptimeInSeconds()));
        nextStepTimeSlider = new VisSlider(0, 5000, 1, false);
        nextStepTimeSlider.setValue(oldConfig.getSteptimeInSeconds());
        nextStepTimeSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (actor instanceof VisSlider) {
                    VisSlider slider = (VisSlider) actor;
                    nextStepTimeValue.setText("" + (int) slider.getValue());
                }
            }
        });

        VisLabel drawHistory = new VisLabel("draw History: ");
        drawHistoryCheckBox = new VisCheckBox("", oldConfig.isDrawHistory());

        VisLabel colorInterpolation = new VisLabel("color Interpolation: ");
        colorInterpolationSelection = new VisSelectBox<String>();
        colorInterpolationSelection.setItems("Linear", "Smooth", "Exponential");

        String interpolationName = "";
        for (EntryOneToOne<String, Interpolation> entry : interpolationMap) {
            if (entry.getT2().equals(oldConfig.getInterpolation())) {
                interpolationName = entry.getT1();
                break;
            }
        }

        colorInterpolationSelection.setSelected(interpolationName);

        VisLabel livingColor = new VisLabel("living creature color: ");
        livingColorPicker = new ColorPickerWindow(oldConfig.getLivingColor());

        VisLabel deadColor = new VisLabel("dead creature color: ");
        deadColorPicker = new ColorPickerWindow(oldConfig.getDeadColor());

        VisLabel unusedColor = new VisLabel("unused creature color: ");
        unusedColorPicker = new ColorPickerWindow(oldConfig.getUnpopulatedColor());

        VisTextButton restartButton = new VisTextButton("apply");
        restartButton.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                bootstrapper.showGame(buildNewConfig());
            }
        });

        VisTextButton restoreButton = new VisTextButton("restore defaults");
        restoreButton.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                bootstrapper.showGame(new GolConfig());
            }
        });
        add(new Image(skin, "GameOfLive_logo")).colspan(3).center().row();
        addSeparator().colspan(3).padTop(-45).padBottom(45).row();
        add(sizeX).padLeft(45);
        add(xSlider);
        add(sizeXValue).width(120).row();
        add(sizeY).padLeft(45);
        add(ySlider);
        add(sizeYValue).width(120).row();
        add(nextStepTime).padLeft(45);
        add(nextStepTimeSlider);
        add(nextStepTimeValue).width(120).row();
        addSeparator().colspan(3).padTop(45).padBottom(45).row();
        add(drawHistory).padLeft(45);
        add(drawHistoryCheckBox).row();
        add(colorInterpolation).padLeft(45);
        add(colorInterpolationSelection).row();
        addSeparator().colspan(3).padTop(45).padBottom(45).row();
        add(livingColor).padLeft(45);
        add(livingColorPicker).row();

        add(deadColor).padLeft(45);
        add(deadColorPicker).row();

        add(unusedColor).padLeft(45);
        add(unusedColorPicker).row();
        addSeparator().colspan(3).padTop(45).padBottom(45).row();
        add(restoreButton).padLeft(45);
        add(restartButton);

        this.pack();
    }

    private GolConfig buildNewConfig() {
        final int sizeX = (int) xSlider.getValue();
        final int sizeY = (int) ySlider.getValue();
        final float steptimeInSeconds = (float) nextStepTimeSlider.getValue() / 1000.0f;
        final boolean drawHistory = drawHistoryCheckBox.isChecked();
        final Color unpopulatedColor = unusedColorPicker.getColor();
        final Color livingColor = livingColorPicker.getColor();
        final Color deadColor = deadColorPicker.getColor();
        Interpolation interpolationByString = Interpolation.linear;
        for (EntryOneToOne<String, Interpolation> entry : interpolationMap) {
            if (entry.getT1().equals(colorInterpolationSelection.getSelected())) {
                interpolationByString = entry.getT2();
                break;
            }
        }
        final Interpolation interpolation = interpolationByString;
        return new GolConfig(sizeX, sizeY, steptimeInSeconds, drawHistory, unpopulatedColor, livingColor, deadColor, interpolation);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    public void show() {
        this.oldConfig = bootstrapper.getConfig();
        this.clear();
        this.buildUI();
        this.addAction(Actions.visible(true));
    }

    public void hide() {
        this.setVisible(false);
        this.addAction(Actions.visible(false));
    }
}
