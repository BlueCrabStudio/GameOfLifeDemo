package com.bluecrabstudio.gameoflifedemo;

public class EntryOneToOne <T1, T2> {
	private T1 element1;
	private T2 element2;
	
	public EntryOneToOne(T1 entry1, T2 entry2) {
		element1 = entry1;
		element2 = entry2;
	}
	public T1 getT1() {
		return element1;
	}
	public T2 getT2() {
		return element2;
	}
	public T2 getByT1(T1 entry) {
		return element2;
	}	
	public T1 getByT2(T2 entry) {
		return element1;
	}
}
