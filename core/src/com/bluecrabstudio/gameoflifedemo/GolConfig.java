package com.bluecrabstudio.gameoflifedemo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;

public class GolConfig {
	private final int sizeX, sizeY;
	private final float steptimeInSeconds;
	private final boolean drawHistory;
	private final Color unpopulatedColor, livingColor, deadColor;
	private final Interpolation interpolation;
	
	public GolConfig() {
		sizeX = Gdx.graphics.getWidth()/2;
		sizeY = Gdx.graphics.getHeight()/2;
		steptimeInSeconds = 0.0f;
		drawHistory = true;

		unpopulatedColor = new Color(0, 0, 0, 1);
		livingColor = new Color(0, 1, 0, 1);
		deadColor = new Color(0, 0, 0, 1);
		interpolation = Interpolation.linear;
	}
	
	public GolConfig(final int sizeX, 
					 final int sizeY,
					 final float steptimeInSeconds,
					 final boolean drawHistory,
					 final Color unpopulatedColor, 
					 final Color livingColor, 
					 final Color deadColor,
					 final Interpolation interpolation) {
		
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.steptimeInSeconds = steptimeInSeconds;
		this.drawHistory = drawHistory;

		this.unpopulatedColor = unpopulatedColor;
		this.livingColor = livingColor;
		this.deadColor = deadColor;
		this.interpolation = interpolation;

		System.out.println(""
				+ "this.sizeX = "+sizeX+";\n"
				+ "this.sizeY = "+sizeY+";\n\n"
				
				+ "this.steptimeInSeconds = "+steptimeInSeconds+";\n"
				+ "this.drawHistory = "+drawHistory+";\n\n"

				+ "this.unpopulatedColor = "+unpopulatedColor+";\n"
				+ "this.livingColor = "+livingColor+";\n"
				+ "this.deadColor = "+deadColor+";\n"
				+ "this.interpolation = "+interpolation+";\n\n");
	}

	public int getSizeY() {
		return sizeY;
	}

	public int getSizeX() {
		return sizeX;
	}

	public float getSteptimeInSeconds() {
		return steptimeInSeconds;
	}

	public boolean isDrawHistory() {
		return drawHistory;
	}

	public Color getUnpopulatedColor() {
		return unpopulatedColor;
	}

	public Color getLivingColor() {
		return livingColor;
	}

	public Color getDeadColor() {
		return deadColor;
	}

	public Interpolation getInterpolation() {
		return interpolation;
	}
}
