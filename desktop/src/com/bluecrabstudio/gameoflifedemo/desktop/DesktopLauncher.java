package com.bluecrabstudio.gameoflifedemo.desktop;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bluecrabstudio.gameoflifedemo.Bootstrapper;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1080;
		config.height = 1920;
		config.resizable = true;
		config.title = "Game Of Life Demo";
        config.addIcon("icon-256.png", FileType.Internal);
        config.addIcon("icon-128.png", FileType.Internal);
        config.addIcon("icon-64.png", FileType.Internal);
        config.addIcon("icon-32.png", FileType.Internal);
		new LwjglApplication(new Bootstrapper(), config);
	}
}
